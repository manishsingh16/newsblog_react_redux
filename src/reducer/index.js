import counter from "./reducer";
import logged from "./logged";
import {combineReducers} from 'redux';

const allreducers = combineReducers({
    counter : counter,
    logged : logged
})


export default allreducers;