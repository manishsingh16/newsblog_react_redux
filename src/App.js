import React from 'react';
import {useSelector, useDispatch} from 'react-redux';



function App() {
const counter = useSelector(state=> state.counter);
const login=useSelector(state => state.logged)
const dispatch=useDispatch();
  return (
    <div className="App">
   <h1>Counter {counter}</h1>
   {login ? <h1>login</h1>: ''}
   <button onClick={() =>dispatch({type: 'increment'})}>+</button>
   <button onClick={()=>dispatch({type: 'decrement'})}>-</button>
    </div>
  );
}

export default App;
